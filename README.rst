=====
Redwood Theme
=====

This is the theme package used for Redwood Labs projects.

Quick start
-----------

1. Add "theme" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'theme',
    ]
